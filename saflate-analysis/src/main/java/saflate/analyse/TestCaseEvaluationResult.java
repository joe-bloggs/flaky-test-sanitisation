package saflate.analyse;

/**
 * Represents the result of evaluating a test.
 * Surefires own flakiness handling is not supported.
 */
public enum TestCaseEvaluationResult {
    failure,error,success,skip
}
