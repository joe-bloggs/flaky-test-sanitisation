package saflate.analyse;

import java.util.Map;

/**
 * Listener to report results.
 */
public interface Reporter {
    enum ReportedDataKind {
        testCount,succeeded, failed, error, failOrError,skipped, flaky, stronglyFlaky, runtime, precision, recall
    }
    void start() ;
    void finish() ;
    void dataAcrossRuns(String experiment,String dataset,String configuration,boolean saflateOn,Map<ReportedDataKind, Object> data);
    void summaryStats(String experiment,String dataset,Map<ReportedDataKind, Object> data);

}
