package saflate.analyse;

import com.google.common.base.Preconditions;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/**
 * Analyse jrip rules produced by a classifier.
 */
public class AnalyseJRipRules {

    public static final String RULES_FOLDER = "rules";
    public static final Logger LOGGER = LogManager.getLogger("saflate-analysis");

    enum Counts {rules, error, failure, success, prereq0, prereq1, prereq2, prereq3,prereq4p}

    public static void main (String[] args) throws Exception {
        Options options = new Options();
        options.addOption(RULES_FOLDER, true, "the folder containing jrip rules");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, args);

        if (!(cmd.hasOption(RULES_FOLDER))) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -cp <classpath> " + Main.class.getName(), "", options, "", true);
            System.exit(1);
        }

        String rulesFolderName = cmd.getOptionValue(RULES_FOLDER);
        File rulesFolder = new File(rulesFolderName);
        Preconditions.checkArgument(rulesFolder.exists());
        Preconditions.checkArgument(rulesFolder.isDirectory());

        for (File ruleDef:rulesFolder.listFiles(f -> f.getName().endsWith("-rules.txt"))) {
            String name = ruleDef.getName().substring(0,ruleDef.getName().indexOf("-rules.txt"));
            LOGGER.info("Analysing: " + name);

            Map<Counts,Integer> counts = new HashMap<>();
            for (Counts c:Counts.values()) {
                counts.put(c,0);
            }
            for (String line:Files.readAllLines(ruleDef.toPath())) {
                if (line.contains("=>")) {
                    String[] parts = line.split("=>");
                    assert parts.length==2;
                    counts.computeIfPresent(Counts.rules,(k,v) -> v+1);

                    // analyse conclusion
                    String concl = parts[1].trim();
                    assert concl.startsWith("teststatus=");
                    if (concl.startsWith("teststatus=error")) {
                        counts.computeIfPresent(Counts.error,(k,v) -> v+1);
                    }
                    else if (concl.startsWith("teststatus=failure")) {
                        counts.computeIfPresent(Counts.error,(k,v) -> v+1);
                    }
                    else if (concl.startsWith("teststatus=success")) {
                        counts.computeIfPresent(Counts.success,(k,v) -> v+1);
                    }

                    // analyse precondition
                    String prem = parts[0].trim();
                    String[] prems = prem.split(" and ");
                    if (prems.length==1) {
                        counts.computeIfPresent(Counts.prereq0,(k,v) -> v+1);
                    }
                    if (prems.length==2) {
                        counts.computeIfPresent(Counts.prereq1,(k,v) -> v+1);
                    }
                    else if (prems.length==3) {
                        counts.computeIfPresent(Counts.prereq2,(k,v) -> v+1);
                    }
                    else if (prems.length==4) {
                        counts.computeIfPresent(Counts.prereq3,(k,v) -> v+1);
                    }
                    else if (prems.length>4) {
                        counts.computeIfPresent(Counts.prereq4p,(k,v) -> v+1);
                    }
                }
            }

            // consistency checks
            assert counts.get(Counts.rules) == (counts.get(Counts.success) + counts.get(Counts.failure) + counts.get(Counts.error));
            assert counts.get(Counts.rules) == (counts.get(Counts.prereq0) + counts.get(Counts.prereq1) + counts.get(Counts.prereq2)  + counts.get(Counts.prereq3)  + counts.get(Counts.prereq4p));
            // report
            for (Counts c:Counts.values()) {
                LOGGER.info("\t" + c.name() + " : " + counts.get(c));
            }
        }



    }
}
