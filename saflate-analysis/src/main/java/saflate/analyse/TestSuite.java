package saflate.analyse;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Represents a test suite containing properties and tests.
 * Part of a simplified domain model defined by https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report.xsd .
 */
public class TestSuite {
    private List<TestCase> testCases = new ArrayList<>();
    private Properties properties = new Properties();
    private double time = 0;

    // for provenance
    private Path report = null;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Path getReport() {
        return report;
    }

    public void setReport(Path report) {
        this.report = report;
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }

    public void addTestCase(TestCase testCase) {
        this.testCases.add(testCase);
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperty(String key,String value) {
        this.properties.setProperty(key,value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestSuite testSuite = (TestSuite) o;
        return Objects.equals(testCases, testSuite.testCases) && Objects.equals(properties, testSuite.properties) && Objects.equals(report, testSuite.report);
    }

    @Override
    public int hashCode() {
        return Objects.hash(testCases, properties, report);
    }
}
