#!/bin/bash
PROG=biojava

COUNT=10
mkdir tmp
mkdir results/$PROG
rm -rf tmp/*
cp dataset/$PROG-saflated.zip mnt/
cd mnt/
mkdir tmp
mkdir results
unzip $PROG-saflated.zip
mv $PROG $PROG-saflated
cp ../dataset/$PROG.zip .
unzip $PROG.zip
rm $PROG.zip
rm $PROG-saflated.zip
cd ..

OUTPUT=testresults-network-$PROG-networkon-saflateoff
DIR=$PROG
docker run --rm -e COUNT=$COUNT -e DISPLAY=$DISPLAY -e OUTPUT=$OUTPUT -e DIR=$DIR -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexpjava11 /bin/bash -c "/mnt/$PROG.sh"

OUTPUT=testresults-network-$PROG-networkon-saflateon
DIR=$PROG-saflated
docker run --rm -e COUNT=$COUNT -e DISPLAY=$DISPLAY -e OUTPUT=$OUTPUT -e DIR=$DIR -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexpjava11 /bin/bash -c "/mnt/$PROG.sh"

OUTPUT=testresults-network-$PROG-networkoff-saflateoff
DIR=$PROG
docker run --rm --network none -e COUNT=$COUNT  -e DISPLAY=$DISPLAY -e OUTPUT=$OUTPUT -e DIR=$DIR -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexpjava11 /bin/bash -c "/mnt/$PROG.sh"

OUTPUT=testresults-network-$PROG-networkoff-saflateon
DIR=$PROG-saflated
docker run --rm --network none -e COUNT=$COUNT -e DISPLAY=$DISPLAY -e OUTPUT=$OUTPUT -e DIR=$DIR -v `pwd`/mnt:/mnt:z --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" saflatexpjava11 /bin/bash -c "/mnt/$PROG.sh"

