package nz.ac.wgtn.saflate.network.junit4;


public interface NetworkExceptionProvider {
    Class<? extends Exception>[] getNetworkExceptionClassesToBeSanitised();
}