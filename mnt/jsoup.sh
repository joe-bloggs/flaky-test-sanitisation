#/bin/bash
counter=1

until [ $counter -gt $COUNT ]
do

  if [[ "$OUTPUT" == *"networkoff"*  ]]; then
    cd /mnt/$DIR
    mvn clean test -fn -o -Dmaven.repo.local=/mnt/cache -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true
  else
    cd /mnt/$DIR
    mvn clean test -Dmaven.repo.local=/mnt/cache -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true
  fi

  cd /mnt/$DIR
  rm /mnt/tmp/* 
  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" /mnt/tmp  \;

  cd /mnt/tmp
  zip -r ../results/$OUTPUT-$counter.zip ./


  ((counter++))

done

