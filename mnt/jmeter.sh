#/bin/bash
counter=1

until [ $counter -gt $COUNT ]
do

  if [[ "$OUTPUT" == *"networkoff"*  ]]; then
    
    /mnt/gradle-6.7.1/bin/gradle -g /mnt/gcache --offline --continue -PchecksumIgnore clean test -Dmaven.repo.local=/mnt/cache
  else
    cd /mnt/$DIR
    /mnt/gradle-6.7.1/bin/gradle -g /mnt/gcache  --continue -PchecksumIgnore clean test -Dmaven.repo.local=/mnt/cache
  fi

  cd /mnt/$DIR
  rm /mnt/tmp/* 
  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" /mnt/tmp  \;

  cd /mnt/tmp
  zip -r ../results/$OUTPUT-$counter.zip ./


  ((counter++))

done

