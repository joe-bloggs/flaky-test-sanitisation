#/bin/bash
counter=1

until [ $counter -gt $COUNT ]
do

  if [[ "$OUTPUT" == *"networkoff"*  ]]; then
    cd /mnt/$DIR
    mvn clean test -fn -o -Dmaven.repo.local=/mnt/cache -f modules/swagger-parser/pom.xml
  else
    cd /mnt/$DIR
    mvn clean test -Dmaven.repo.local=/mnt/cache -f modules/swagger-parser/pom.xml
  fi

  cd /mnt/$DIR
  rm /mnt/tmp/* 
  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" /mnt/tmp  \;

  cd /mnt/tmp
  zip -r ../results/$OUTPUT-$counter.zip ./


  ((counter++))

done

