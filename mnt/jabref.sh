#/bin/bash
counter=1

until [ $counter -gt $COUNT ]
do

  if [[ "$OUTPUT" == *"networkoff"*  ]]; then
    cd /mnt/$DIR
    /mnt/gradle-7.3.3/bin/gradle -g /mnt/gcache --offline --continue -PchecksumIgnore clean test -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true
  else
    cd /mnt/$DIR
    /mnt/gradle-7.3.3/bin/gradle -g /mnt/gcache  --continue -PchecksumIgnore clean test -Djunit.jupiter.extensions.autodetection.enabled=true -Dsaflate.supportconcurrent-test-execution=true
  fi

  cd /mnt/$DIR
  rm /mnt/tmp/* 
  find . -type f -regex '.*TEST.*.xml'  -exec cp "{}" /mnt/tmp  \;

  cd /mnt/tmp
  zip -r ../results/$OUTPUT-$counter.zip ./


  ((counter++))

done

