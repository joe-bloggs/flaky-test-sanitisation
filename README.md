## README

### Prerequisites

All experiments are run inside docker containers. Docker must be installed and running before building any image. Download and install docker from here: https://docs.docker.com/get-docker/


### Project structure
This repository contains several modular saflate  packages (each contains a seperate README file):

+ **saflate/saflate-network-junit4**: junit4 rules to sanitise tests that are flaky due to network issues.

+ **saflate/saflate-network-junit5**: junit5 rules to sanitise tests that are flaky due to network issues.

+ **saflate-analysis**: scripts to parse experiment results (folders or zip files containing surefilre reports), and output a summary of results including runtimes and precision/false positive and recall/false negative analysis to the console.

+ **results**: results of test reports.

### Building docker images

To build all docker images, run `build-images.sh`

### Running Experiments

Scripts for rerunning the experiments are in `./scripts` folder. For example, to run the experiment for jsoup: `./scripts/jsoup.sh`

### Inspecting Results

The build results  will be captured and stored in a file that uses the naming convention: `testresults-network-<program>-network<on|off>-saflate<on|off>, for example:

`testresults-network-jsoup-networkon-saflateoff-1.zip`

Results for network flakiness obtained from multiple reruns (10 reruns for each run confirmation per program) are available under `./results/[project]/`

